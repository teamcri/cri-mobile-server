<?php
set_exception_handler('exception_handler');
function exception_handler($e) {
    echo "Error: " . $e->getMessage() . " on line " . $e->getLine() . "\n";
}
date_default_timezone_set("US/Eastern");
class ACS
{
    private $key;
    private $session_id;

    function __construct($key) {
        $this->key = $key;
        $this->session_id = $this->login();
        if (($this->session_id) === 0) {throw new Exception("Login Error");}
    }

    private function login()
    {
        /*** SETUP ***************************************************/
        $username = "cri-web";
        $password = "qq19k6LvLh";
        $json = '{"alert":"' . "" . '","title":"' . "" . '","vibrate":true,"sound":"default"}';


        /*** INIT CURL *******************************************/
        $curlObj = curl_init();
        $c_opt = array(CURLOPT_URL => 'https://api.cloud.appcelerator.com/v1/users/login.json?key=' . $this->key,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => 1,
            CURLOPT_POSTFIELDS => "login=" . $username . "&password=" . $password,
            CURLOPT_FOLLOWLOCATION => 1,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_TIMEOUT => 60
        );

        /*** LOGIN **********************************************/
        curl_setopt_array($curlObj, $c_opt);

        $result = curl_exec($curlObj);
        if (curl_errno($curlObj)) throw new Exception(curl_error($curlObj));


        /*** THE END ********************************************/
        curl_close($curlObj);

        $result = json_decode($result);
        if ($result->meta->code == 200) {
            return $result->meta->session_id;
        } else {
            return 0;
        }
    }


    function get_all()
    {
        $skip = 0;
        $limit = 1000;
        $done = false;
        $events = [];

        $curl = curl_init();

        while (!$done) {

            curl_setopt_array($curl, array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => 'https://api.cloud.appcelerator.com/v1/events/query.json?key=' . $this->key . '&count=true&limit=' . $limit . '&skip=' . $skip . '&new_pagination=true',
                CURLOPT_SSL_VERIFYPEER => false
            ));
            $response = curl_exec($curl);

            if (curl_errno($curl)) {
                $error = curl_error($curl);
            } else {
                $response = json_decode($response, true);
                $total = $response["meta"]["count"];
                $eventCount = count($response["response"]["events"]);
                $events = array_merge($events, $response["response"]["events"]);
                if (count($events) >= $total) {
                    $done = true;
                }
                $skip += $eventCount;
            }
        }

        curl_close($curl);
        return $events;
    }

    function getEvent($id) {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => 'https://api.cloud.appcelerator.com/v1/events/query.json?key=' . $this->key . '&where=%7B%22gCalID%22%3A%22' . $id . '%22%7D&new_pagination=true&count=true',
            CURLOPT_SSL_VERIFYPEER => false
        ));
        $response = curl_exec($curl);

        if (curl_errno($curl)) {
            $error = curl_error($curl);
            curl_close($curl);
            return 0;
        } else {
            curl_close($curl);
            $response = json_decode($response, true);
            if ($response["meta"]["code"] == 200) {
                $total = $response["meta"]["count"];
                if($total == 1) {
                    return  $response["response"]["events"];
                } else {
                    return 0;
                }
            } else {
                return 0;
            }
        }

    }

    function update($event, $id) {
        $event[] = $event;
        $curl = curl_init();
        $c_opt = array(
            CURLOPT_URL => 'https://api.cloud.appcelerator.com/v1/events/update.json?key=' . $this->key . '&_session_id=' . $this->session_id . '&event_id=' . $id,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => 1,
            CURLOPT_POSTFIELDS => http_build_query($event),
            CURLOPT_FOLLOWLOCATION => 1,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_TIMEOUT => 60,
            CURLOPT_CUSTOMREQUEST => "PUT"

        );
        curl_setopt_array($curl, $c_opt);
        $response = curl_exec($curl);

        if (curl_errno($curl)) {
            $error = curl_error($curl);
            curl_close($curl);
            return $error;
        } else {
            curl_close($curl);
            $response = json_decode($response, true);
            $code = $response["meta"]["code"];
            if ($code == 200) {
                return  "Update Success: " . $event[0]["name"];
            } else {
                return "Error " . $code . ": " . $response["meta"]["status"];
            }
        }
    }

    function batchUpdate($events) {
        $results = [];
        foreach($events as $event) {
            $id = json_decode($event["custom_fields"], true);
            $id = $id["gCalID"];
            $eventId = $this->getEvent($id);;
            $exists = $eventId != 0 ? true : false;

            if ($exists) {
                $id = $eventId[0]["id"];
               $results[] = $this->update($event, $id);
            } else {
                $send[] = $event;
                $results[] = implode(",",$this->create_events($send));
                $send = [];
            }
        }
        return $results;
    }

    function create_events($data) {
        $results = [];
        $curl = curl_init();
        foreach($data as $event) {

            $c_opt = array(CURLOPT_URL => 'https://api.cloud.appcelerator.com/v1/events/create.json?key=' . $this->key.'&_session_id='.$this->session_id,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_POST => 1,
                CURLOPT_POSTFIELDS => http_build_query($event),
                CURLOPT_FOLLOWLOCATION => 1,
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_TIMEOUT => 60

            );
            curl_setopt_array($curl, $c_opt);
            $response = curl_exec($curl);

            if (curl_errno($curl)) {
                $error = curl_error($curl);
                curl_close($curl);
                return $error;
            } else {
                $response = json_decode($response, true);
                $code = $response["meta"]["code"];
                if ($code == 200) {
                    $results[] = "Create Success: " . $event["name"];
                } else {
                    $results[] =  "Error " . $code . ": " . $response["meta"]["status"];
                }
            }

        }
        curl_close($curl);
        return $results;
    }

    function delete_all()
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => 'https://api.cloud.appcelerator.com/v1/events/batch_delete.json?key=' . $this->key . '&_session_id=' . $this->session_id,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_CUSTOMREQUEST => "DELETE"
        ));

        $response = curl_exec($curl);

        if (curl_errno($curl)) {
            $error = curl_error($curl);
            curl_close($curl);
            return $error;
        } else {
            $response = json_decode($response, true);
            curl_close($curl);
            $code = $response["meta"]["code"];
            if ($code == 200) {
                return "Success";
            } else {
                return "Error " . $code . ": " . $response["meta"]["status"];
            }
        }

    }


    function batchDelete($events){
        $results = [];
        foreach($events as $event) {
            $id = $event["id"];
            $eventId = $this->getEvent($id);;
            $exists = $eventId != 0 ? true : false;
            if ($exists) {
                $id = $eventId[0]["id"];
                $results[] = $this->delete($id);
            }
        }
        return $results;
    }

    function delete($id)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => 'https://api.cloud.appcelerator.com/v1/events/delete.json?key=' . $this->key . '&_session_id=' . $this->session_id . '&event_id=' . $id,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_CUSTOMREQUEST => "DELETE"
        ));

        $response = curl_exec($curl);

        if (curl_errno($curl)) {
            $error = curl_error($curl);
            curl_close($curl);
            return $error;
        } else {
            curl_close($curl);
            $response = json_decode($response, true);
            $code = $response["meta"]["code"];
            if ($code == 200) {
                return "Success";
            } else {
                return "Error " . $code . ": " . $response["meta"]["status"];
            }
        }


    }

}
//cHh9DGat3bUdWqvKFUxxH62lAV1yyUSE
/*try {
    $production = new ACS("cHh9DGat3bUdWqvKFUxxH62lAV1yyUSE");
} catch (Exception $e) {
    echo $e->getMessage();
}

if ($production) {

    echo $production->delete("53ebc01068e15bb4d802944d");


}*/