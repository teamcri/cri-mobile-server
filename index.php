<?php
date_default_timezone_set("US/Eastern");
include_once("Calendar.php");
include_once("ACS.php");
set_time_limit (0);

$calendars = new Calendar();
$calList = $calendars->keys_calendars;

try {
//    $acs = new ACS("YIgBsGXMJzBojcOJwsCrddhKVbChyml0");
    $acs = new ACS("cHh9DGat3bUdWqvKFUxxH62lAV1yyUSE");
} catch (Exception $e) {
    echo $e->getMessage();
}

if ($acs) {
//    $events = $calendars->getSingleCalendar("Intramurals");
//    $events = $calendars->getAllEvents();
    $events = $calendars->changed_events("2015-01-08T16:11:44.462Z");
    print_r($events);
//    $result = $acs->create_events($events);
//    $result = $acs->delete_all();
//    print_r($result);
    print_r($events);
}

//print_r($calendars->getSingleCalendar("Fitness"));
?>


<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>App Interface</title>
</head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
<script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>

<body>
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <hr/>
            <form action="">
                <select class="form-control">
                    <?php foreach($calList as $key=>$cal): ?>
                    <option> <?= $key ?> </option>
                    <?php endforeach; ?>
                </select>
            </form>
            <br/>
            <button type="button" class="btn btn-primary">Send to Server</button>
            <button type="button" class="btn btn-primary">Default button</button>

        </div>
    </div>
</div>
</body>
</html>