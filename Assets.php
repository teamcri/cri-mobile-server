<?php

class Assets
{

     public function getEvents($page){

         $all_events = array();

        // 1. initialize
        $ch = curl_init();

        // 2. set the options, including the url
        curl_setopt($ch, CURLOPT_URL, "https://api.cloud.appcelerator.com/v1/events/query.json?key=cHh9DGat3bUdWqvKFUxxH62lAV1yyUSE&response_json_depth=1&order=start_time&per_page=50&page=".$page);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);

        // 3. execute and fetch the resulting HTML output
        $output = curl_exec($ch);
        $output = json_decode($output, true);
        $events = $this->formatDisplayEvents($output["response"]["events"]);
        $all_events = array_merge($all_events, $events);
        // 4. free up the curl handle
        curl_close($ch);

        return $all_events;

    }


    static public function getSocial($page){

        // 1. initialize
        $ch = curl_init();

        // 2. set the options, including the url
        curl_setopt($ch, CURLOPT_URL, "https://api.cloud.appcelerator.com/v1/posts/query.json?key=cHh9DGat3bUdWqvKFUxxH62lAV1yyUSE&response_json_depth=1&per_page=50&page=".$page);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);

        // 3. execute and fetch the resulting HTML output
        $output = curl_exec($ch);
        $output = json_decode($output, true);
        // 4. free up the curl handle
        curl_close($ch);
        return $output;

    }

    public function formatDisplayEvents($events_list) {
//    print_r($event);
        $events = array();

        foreach ($events_list as $event) {
//            $date = date("F j, Y, g:i a", $event["start_time"]);
            $date = new DateTime($event["start_time"]);
            //echo $date;
            $newEvent = array(
                "name"       => $event["name"],
                "start_time" => $date->format("F j, Y, g:i A"),
                "details"    => array_key_exists("details", $event) ? $event["details"] : '',
                "id"         => $event["id"],
                "calendar"   => $event["custom_fields"]["gCal"],
                "image_url"  => $event["custom_fields"]["image_url"],
                "video_url"  => $event["custom_fields"]["video_url"],
                "location"   => $event["custom_fields"]["location"]
            );

            $events[] = $newEvent;

        } //End foreach



        return $events;

    }
}

