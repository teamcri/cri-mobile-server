<?php
date_default_timezone_set("US/Eastern");
class Calendar
{
    private $regex_url = "/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/";

    private $calendars = array(
        "georgiasouthern.edu_rb399jhvctp8nivasdsea28mhc@group.calendar.google.com", //Aquatics
        "georgiasouthern.edu_94h9bq4h8up69sb1qcmvcfcs6c@group.calendar.google.com", //Club Sports
        "georgiasouthern.edu_dhj6v9kumt4h8kuu75gmafvl8g@group.calendar.google.com", //Fitness
        "georgiasouthern.edu_cjvte204nh60aiugu0f9uvuv8c@group.calendar.google.com", //Intramurals
        "georgiasouthern.edu_6je7loq7eun6cgq0mf8bsb03l4@group.calendar.google.com", //Golf
        "georgiasouthern.edu_e6s0da4p1v0guvjcd433jvgc9s@group.calendar.google.com", //Southern Adventures
        "georgiasouthern.edu_hlp8t9iv0gdnpe1ie2p2tbjpto@group.calendar.google.com", //Wellness
        "georgiasouthern.edu_4m7a4lbcmgv0f4kce0lg6a8ce8@group.calendar.google.com", //Group Fitness
        "cri-web@georgiasouthern.edu" //CRI
    );

    public $keys_calendars = array(
        "Aquatics" => "georgiasouthern.edu_rb399jhvctp8nivasdsea28mhc@group.calendar.google.com", //Aquatics
        "Club Sports" => "georgiasouthern.edu_94h9bq4h8up69sb1qcmvcfcs6c@group.calendar.google.com", //Club Sports
        "Fitness" => "georgiasouthern.edu_dhj6v9kumt4h8kuu75gmafvl8g@group.calendar.google.com", //Fitness
        "Intramurals" => "georgiasouthern.edu_cjvte204nh60aiugu0f9uvuv8c@group.calendar.google.com", //Intramurals
        "Golf" => "georgiasouthern.edu_6je7loq7eun6cgq0mf8bsb03l4@group.calendar.google.com", //Golf
        "Southern Adventures" => "georgiasouthern.edu_e6s0da4p1v0guvjcd433jvgc9s@group.calendar.google.com", //Southern Adventures
        "Wellness" => "georgiasouthern.edu_hlp8t9iv0gdnpe1ie2p2tbjpto@group.calendar.google.com", //Wellness
        "Group Fitness" => "georgiasouthern.edu_4m7a4lbcmgv0f4kce0lg6a8ce8@group.calendar.google.com", //Group Fitness
        "CRI" => "cri-web@georgiasouthern.edu" //CRI
    );


    public function changed_events($last_sync) {
        $all_events = array();

        // 1. initialize
        $ch = curl_init();
        foreach($this->calendars as $calendar) {

            $date =  $last_sync;


            // 2. set the options, including the url
            curl_setopt($ch, CURLOPT_URL, "https://www.googleapis.com/calendar/v3/calendars/" . $calendar ."/events?alwaysIncludeEmail=false&maxResults=1000&singleEvents=true&updatedMin=" . urlencode($date) . "&key=AIzaSyC-6t5AZUcivDzq-5HsEJu_Lk39W0nf5rA");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);


            // 3. execute and fetch the resulting HTML output
            $output = curl_exec($ch);
            $output = json_decode($output, true);
            if(!empty($output["items"])){
                $all_events = array_merge($all_events, $output["items"]);
            }
        }
        // 4. free up the curl handle
        curl_close($ch);

        return $all_events;
    }

    public function getAllEvents() {

        $all_events = array();
        $date =  date("c");
        // 1. initialize
        $ch = curl_init();
        foreach($this->calendars as $calendar) {

            // 2. set the options, including the url
            curl_setopt($ch, CURLOPT_URL, "https://www.googleapis.com/calendar/v3/calendars/" . $calendar ."/events?alwaysIncludeEmail=false&maxResults=1000&orderBy=startTime&singleEvents=true&timeMin=" . urlencode($date) . "&key=AIzaSyC-6t5AZUcivDzq-5HsEJu_Lk39W0nf5rA");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);


            // 3. execute and fetch the resulting HTML output
            $output = curl_exec($ch);
            $output = json_decode($output, true);
            $events = $this->formatEvents($output["items"]);
            $all_events = array_merge($all_events, $events);

        }
        // 4. free up the curl handle
        curl_close($ch);
        return $all_events;

    }


    public function getSingleCalendar($calendarString) {
        $calendar = $this->keys_calendars[$calendarString];

        if($calendar) {

            $date =  date("c");

            // 1. initialize
            $ch = curl_init();

            // 2. set the options, including the url
            curl_setopt($ch, CURLOPT_URL, "https://www.googleapis.com/calendar/v3/calendars/" . $calendar ."/events?alwaysIncludeEmail=false&maxResults=1000&orderBy=startTime&singleEvents=true&timeMin=" . urlencode($date) . "&key=AIzaSyC-6t5AZUcivDzq-5HsEJu_Lk39W0nf5rA");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);


            // 3. execute and fetch the resulting HTML output
            $output = curl_exec($ch);
            $output = json_decode($output, true);
            $events = $this->formatEvents($output["items"]);
            // 4. free up the curl handle
            curl_close($ch);
            return $events;

        } else {
            return false;
        }
    }


    public function getSingleCalendarRaw($calendarString) {
        $calendar = $this->keys_calendars[$calendarString];

        if($calendar) {

            $date =  date("c");

            // 1. initialize
            $ch = curl_init();

            // 2. set the options, including the url
            curl_setopt($ch, CURLOPT_URL, "https://www.googleapis.com/calendar/v3/calendars/" . $calendar ."/events?alwaysIncludeEmail=false&maxResults=1000&orderBy=startTime&singleEvents=true&timeMin=" . urlencode($date) . "&key=AIzaSyC-6t5AZUcivDzq-5HsEJu_Lk39W0nf5rA");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

            // 3. execute and fetch the resulting HTML output
            $output = curl_exec($ch);
            $output = json_decode($output, true);
            curl_close($ch);
            return $output;

        } else {
            return false;
        }
    }


    //Parsing methods

    public function parseVideoUrl($text)
    {

        if(preg_match($this->regex_url, $text, $url)) {

            if($this->isYoutube($url[0])) {
                return $url[0];
            } else {
                return null;
            }

        } else {

            return null;

        }

    }

    public function parseImageUrl($text)
    {

        if(preg_match($this->regex_url, $text, $url)) {

            if($this->isImage($url[0]) && $this->isDriveImage($url[0])) {
                return $url[0];
            } else {
                return null;
            }

        } else {

            return null;
        }

    }

    public function cleanText($text) {
        if($text){
            return preg_replace('@((https?://)?([-\w]+\.[-\w\.]+)+\w(:\d+)?(/([-\w/_\.]*(\?\S+)?)?)*)@', '', $text);
        } else {
            return null;
        }
    }


    public function isImage($url)
    {
        $data = pathinfo($url);
        $ext = strtolower($data['extension']);
        return in_array($ext, array("jpg", "jpeg", "png", "gif"));
    }

    public function isYoutube($url)
    {
        $parse = parse_url($url);
        $host = $parse["host"];
        if($host == "youtube.com"){
            return true;
        } else {
            return false;
        }
    }

    public function isDriveImage($url)
    {
        $parse = parse_url($url);
        $host = $parse["host"];
        if($host == "googledrive.com"){
            return true;
        } else {
            return false;
        }

    }

    public function cleanString($string) {
        $string = strtolower($string);
        //Make alphanumeric (removes all other characters)
        $string = preg_replace("/[^a-z0-9_\s-]/", "", $string);
        //Clean up multiple dashes or whitespaces
        $string = preg_replace("/[\s-]+/", " ", $string);
        //Convert whitespaces and underscore to dash
        $string = preg_replace("/[\s_]/", "_", $string);
        return $string;
    }

    public function formatEvents($events_list) {
//    print_r($event);
        $events = array();

         foreach ($events_list as $event) {
            $start = array_values($event["start"]);
             $start = new DateTime($start[0]);
             $start = $start->format('Y-m-d H:i');
            $end = array_values($event["end"]);

            $newEvent = array(
                "name"       => $event["summary"],
                "start_time" => $start,
                "details"    =>  isset($event["description"]) ? $this->cleanText($event["description"]) : "",
                "custom_fields" => json_encode([
                    "etag" => $this->cleanString($event["etag"]),
                    "gCalID" => $event["id"],
                    "end_time"   => $end[0],
                    "location" => isset($event["location"]) ? $event["location"] : "" ,
                    "gCal" => $this->cleanString($event["organizer"]["displayName"]),
                    "image_url" => isset($event["description"]) ? $this->parseImageUrl($event["description"]) : "",
                    "video_url" => isset($event["description"]) ? $this->parseVideoUrl($event["description"]) : ""
                ])
            );

            $events[] = $newEvent;

        } //End foreach



        return $events;

    }
}